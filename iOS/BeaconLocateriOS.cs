﻿using System;
using mh2mall;
using Xamarin.Forms;
using mh2mall.iOS;
using CoreLocation;
using Foundation;
using UIKit;
using System.Collections.Generic;
using AudioToolbox;
using AVFoundation;

[assembly: Dependency (typeof(BeaconLocateriOS))]

namespace mh2mall.iOS
{
	public class BeaconLocateriOS : IBeaconLocater
	{
		CLLocationManager locationManager;

		string beaconUuid = "BAF8FD00-7674-445E-846B-9958BF6E944A";
		string beaconId = "mhBeacon";
		CLBeaconRegion beaconRegion;

		List<BeaconItem> beacons;

		//BeaconItem theBeacon = new BeaconItem ();

		bool trackingMode = false;

		CLProximity currentProximity;

		//bool alertUp = false;

		String beaconEnterNotificationText;
		String beaconExitNotificationText;
		DateTime lastEnterNotification;
		DateTime lastExitNotification;

		public BeaconLocateriOS ()
		{
		}

		public void startBeacon (String buuid, String bid, String notifTitle, String enterNotifText, String exitNotifText)
		{
			beaconUuid = buuid;
			beaconId = bid;
			beaconEnterNotificationText = enterNotifText;
			beaconExitNotificationText = exitNotifText;

			SetupBeaconRanging ();

			locationManager.StartMonitoring (beaconRegion);
			locationManager.RequestState (beaconRegion);

		}

		public List<BeaconItem> GetAvailableBeacons ()
		{
			for(int i = 0;i < beacons.Count;i++)
			{
				if((DateTime.Now - beacons[i].lastSeen).TotalSeconds > 10)
				{
					if(beacons [i].gone == false)
					{
						beacons [i].gone = true;
						beacons [i].goneTime = DateTime.Now;
					}
				} 
				if((DateTime.Now - beacons[i].goneTime).TotalSeconds > 10 && beacons[i].gone)
				{
					Console.WriteLine ("REMOVE BEACONS GONE");
					beacons.Remove(beacons[i]);
				} 
			}
			return beacons;
		}
			
		public void setTracking(bool track)
		{
			trackingMode = track;
		}

		private void SetupBeaconRanging ()
		{
			Console.WriteLine ("SetupBeaconRanging");

			locationManager = new CLLocationManager ();

			Console.WriteLine (CLLocationManager.Status);

			//locationManager.RequestWhenInUseAuthorization ();
			locationManager.RequestAlwaysAuthorization();

			beacons = new List<BeaconItem> ();

			var rUuid = new NSUuid (beaconUuid);
			beaconRegion = new CLBeaconRegion (rUuid, beaconId);

			beaconRegion.NotifyEntryStateOnDisplay = true;
			beaconRegion.NotifyOnEntry = true;
			beaconRegion.NotifyOnExit = true;

			locationManager.RegionEntered += HandleRegionEntered;
			locationManager.RegionLeft += HandleRegionLeft;
			locationManager.DidDetermineState += HandleDidDetermineState;
			locationManager.DidRangeBeacons += HandleDidRangeBeacons;
		}

		void HandleRegionLeft (object sender, CLRegionEventArgs e)
		{
			if (e.Region.Identifier.Equals (beaconId)) {
				locationManager.StopRangingBeacons (beaconRegion);

				if(beaconExitNotificationText != "")
				{
					lastExitNotification = DateTime.Now;

					var notification = new UILocalNotification ();
					notification.AlertBody = beaconExitNotificationText;
					//notification.ApplicationIconBadgeNumber = 4;
					notification.SoundName = UILocalNotification.DefaultSoundName;
					UIApplication.SharedApplication.PresentLocalNotificationNow (notification);
				}

			} 
		}

		void HandleRegionEntered (object sender, CLRegionEventArgs e)
		{
			Console.WriteLine ("Region entered: " + e.Region.Identifier+" "+beaconId);
			if (e.Region.Identifier.Equals (beaconId)) {
				locationManager.StartRangingBeacons (beaconRegion);
				//var notification = new UILocalNotification { AlertBody = "Svart is in range" };
				//UIApplication.SharedApplication.PresentLocalNotificationNow (notification);

				if(beaconEnterNotificationText != "")
				{
					lastEnterNotification = DateTime.Now;

					var notification = new UILocalNotification ();
					notification.AlertBody = beaconEnterNotificationText;
					//notification.ApplicationIconBadgeNumber = 4;
					notification.SoundName = UILocalNotification.DefaultSoundName;
					UIApplication.SharedApplication.PresentLocalNotificationNow (notification);
				}
			} 
		}

		void HandleDidDetermineState (object sender, CLRegionStateDeterminedEventArgs e)
		{
			if (e.Region.Identifier.Equals (beaconId)) {
				if (e.State == CLRegionState.Inside) {
					Console.WriteLine ("Inside beacon region [{0}]", e.Region.Identifier);
					locationManager.StartRangingBeacons (beaconRegion);

				} else if (e.State == CLRegionState.Outside) {
					Console.WriteLine ("Outside beacon region");
					locationManager.StopRangingBeacons (beaconRegion);

					// TODO: Remove beacon from list
				}
			} 
		}

		void HandleDidRangeBeacons (object sender, CLRegionBeaconsRangedEventArgs e)
		{
			if (e.Beacons.Length > 0) {
				foreach (var b in e.Beacons) {

					if (b.Proximity != CLProximity.Unknown) {
						//Console.WriteLine ("UUID: {0} | Major: {1} | Minor: {2} | Accuracy: {3} | Proximity: {4} | RSSI: {5}", b.ProximityUuid, b.Major, b.Minor, b.Accuracy, b.Proximity, b.Rssi);
						var exists = false;
						for (int i = 0; i < beacons.Count; i++) {
							if (beacons [i].Minor.Equals (b.Minor.ToString ()) && beacons [i].Major.Equals (b.Major.ToString ())) {
								beacons [i].CurrentDistance = Math.Round (b.Accuracy, 2);
								SetProximity (b, beacons [i]);
								beacons [i].gone = false;
								beacons [i].lastSeen = DateTime.Now;
								exists = true;
							}
						}

						if (!exists) {
							var newBeacon = new BeaconItem {
								Minor = b.Minor.ToString (),
								Major = b.Major.ToString(), 
								Name = "", 
								CurrentDistance = Math.Round (b.Accuracy, 2),
								gone = false,
								lastSeen = DateTime.Now
							};
							SetProximity (b, newBeacon);
							beacons.Add (newBeacon);
						}
					}
				}
			}
		}
			
		void SetProximity (CLBeacon source, BeaconItem dest)
		{

			Proximity p = Proximity.Unknown;

			switch (source.Proximity) {
			case CLProximity.Immediate:
				p = Proximity.Immediate;
				break;
			case CLProximity.Near:
				p = Proximity.Near;
				break;
			case CLProximity.Far:
				p = Proximity.Far;
				break;
			}

			if (p > dest.Proximity || p < dest.Proximity) {
				dest.ProximityChangeTimestamp = DateTime.Now;
			} 

			dest.Proximity = p;
		}

	}
}

