﻿using System;
using System.Collections.Generic;
using mh2mall;
using mh2mall.iOS;
using Xamarin.Forms;

using Xamarin.InAppPurchase;
using Xamarin.InAppPurchase.Utilities;

using Foundation;
using UIKit;

using System.Threading.Tasks;

[assembly: Dependency (typeof(IIAPios))]
namespace mh2mall.iOS
{
	public class IIAPios : IIAP
	{
		
		List<InAppProduct> products = new List<InAppProduct> ();
		List<iapProduct> inapp_products = new List<iapProduct> ();
		List<string> getProducts;
		InAppPurchaseManager iap = new InAppPurchaseManager();

		bool loadingProducts = false;
		int buyResult = 0;
		bool restoring = false;

		public IIAPios ()
		{
			Console.WriteLine ("IIAP created");

			if (iap.CanMakePayments) {
				Console.WriteLine ("CAN PAY");
			} else {
				Console.WriteLine ("CANNOT PAY");
			}

			getProducts = new List<string>() {"mh2goldcoin", "mh2premium"};

			iap.ReceivedInvalidProducts += (string[] productIDs) => {

				Console.WriteLine("GOT INVALID PRODUCTS");

				Console.WriteLine(productIDs);
			};

			iap.ReceivedValidProducts += (receivedProducts) => {
				// Received valid products from the iTunes App Store,

				Console.WriteLine("GOT PRODUCTS");

				Console.WriteLine(receivedProducts.Count);

				inapp_products.Clear();

				foreach (InAppProduct product in receivedProducts) {
					// Take action based on the product type
					switch (product.ProductType) {
					case InAppProductType.Consumable:
						// Consumable products can always be purchased again
						products.Add (product);
						inapp_products.Add (new iapProduct(product.ProductIdentifier, product.Title, (float)product.Price, product.FormattedPrice, product.Description, product.Purchased));
						break;
					case InAppProductType.AutoRenewableSubscription:
					case InAppProductType.NonRenewingSubscription:
						// Only display if the subscription has expired
						if (product.SubscriptionExpired)
						{
							products.Add (product);
							inapp_products.Add (new iapProduct(product.ProductIdentifier, product.Title, (float)product.Price, product.FormattedPrice, product.Description, product.Purchased));
						}
						break;
					default:
						// Only display if the product hasn't been purchased
						//if (!product.Purchased)
						products.Add (product);
						inapp_products.Add (new iapProduct(product.ProductIdentifier, product.Title, (float)product.Price, product.FormattedPrice, product.Description, product.Purchased));
						break;
					}
				}
				Console.WriteLine("GOT PRODUCTS DONE");
				loadingProducts = false;
			};

			iap.InAppProductPurchaseFailed += (transaction, product) => {
				// Inform caller that the purchase of the requested product failed.
				buyResult = 2;
			};

			iap.InAppProductPurchased += (transaction, product) => {
				// Update list to remove any non-consumable products that were
				// purchased
				buyResult = 1;
			};

			iap.InAppPurchasesRestored += (count) => {
				// Update list to remove any non-consumable products that were
				// purchased and restored

				restoring = false;
			};

			iap.InAppProductPurchaseUserCanceled += (transaction, product) => {
				buyResult = 2;
			};
		}



		public async Task<List<iapProduct>> loadProducts()
		{
			loadingProducts = true;
			iap.QueryInventory (getProducts);

			int loopCounter = 0;
			while(loadingProducts)
			{
				await Task.Delay (500);
				loopCounter++;
				if(loopCounter > 20)
				{
					return null;
				}
			}

			return inapp_products;
		}

		public async Task<bool> buyProduct(String productId)
		{
			buyResult = 0;
			iap.BuyProduct (productId);

		
			int loopCounter = 0;
			while(buyResult == 0)
			{
				await Task.Delay (500);
				loopCounter++;
				if(loopCounter > 10000)
				{
					return false;
				}
			}

			if (buyResult == 1) {
				return true;
			} else {
				return false;
			}
				
			return false;
		}

		public async Task<bool> restoreProducts()
		{
			restoring = true;
			iap.RestorePreviousPurchases ();

			int loopCounter = 0;
			while(restoring)
			{
				await Task.Delay (500);
				loopCounter++;
				if(loopCounter > 20)
				{
					return false;
				}
			}

			return true;
		}

	}
}

