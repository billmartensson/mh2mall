﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

using UserNotifications;

using Firebase.Analytics;
using Firebase.CloudMessaging;

namespace mh2mall.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();

			// KOD START
			Firebase.Analytics.App.Configure();
			// Register your app for remote notifications.
			if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
			{
				// iOS 10 or later
				var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
				UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) =>
				{
					Console.WriteLine(granted);
				});

				// For iOS 10 display notification (sent via APNS)
				//UNUserNotificationCenter.Current.Delegate = this;

				// For iOS 10 data message (sent via FCM)
				//Messaging.SharedInstance.RemoteMessageDelegate = this;
			}
			else {
				// iOS 9 or before
				var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
				var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
				UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
			}

			UIApplication.SharedApplication.RegisterForRemoteNotifications();

			Messaging.SharedInstance.Connect(error =>
			{
				if (error != null)
				{
					// Handle if something went wrong while connecting
				}
				else {
					// Let the user know that connection was successful
				}
			});

			// KOD SLUT

			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}

		public override void DidEnterBackground(UIApplication application)
		{
			Messaging.SharedInstance.Disconnect();
		}
		public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
			Messaging.SharedInstance.AppDidReceiveMessage(userInfo);
		}


	}


}
