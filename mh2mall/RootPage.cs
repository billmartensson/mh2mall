﻿using System;

using Xamarin.Forms;

namespace mh2mall
{
	public class RootPage : MasterDetailPage
	{
		public RootPage()
		{
			Master = new MenuPage(this) { Title = "Menu" };
			Detail = new NavigationPage(new StartPage());
		}
	}
}

