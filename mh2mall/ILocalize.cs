﻿using System;
using System.Globalization;

namespace mh2mall
{
	public interface ILocalize
	{
		CultureInfo GetCurrentCultureInfo();
		void SetLocale(CultureInfo ci);
	}
}

