﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace mh2mall
{
	public interface IBeaconLocater
	{
		List<BeaconItem> GetAvailableBeacons();

		void setTracking (bool track);
		void startBeacon (String buuid, String bid, String notifTitle, String enterNotifText, String exitNotifText);
	}
}

