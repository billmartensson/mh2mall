﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using Newtonsoft.Json.Linq;

using Plugin.GoogleAnalytics;

namespace mh2mall
{
	public class product
	{
		public string productId { get; set; }
		public string category { get; set; }
		public string name { get; set; }
		public string price { get; set; }
		public string image { get; set; }
		public string description { get; set; }
		public int favCounter { get; set; }
		public string major { get; set; }
	}

	public partial class ListProductsPage : ContentPage
	{
		List<product> products = new List<product>();

		string categoryName = "";
		int totalAmount = 0;

		public ListProductsPage(string category)
		{
			InitializeComponent();

			categoryName = category;

			GoogleAnalytics.Current.Tracker.SendView("Products/"+categoryName);


			productsList.ItemSelected += clickedRow;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			productsList.ItemsSource = null;

			if (categoryName == "")
			{
				// Hämta sparad favoritlista
				if (App.Current.Properties.ContainsKey("fav"))
				{
					products = App.Current.Properties["fav"] as List<product>;

					totalAmount = 0;
					foreach (var prod in products)
					{
						totalAmount = totalAmount + int.Parse(prod.price);
					}
					buyButton.Text = "Köp! " + totalAmount.ToString() + "kr";

					productsList.ItemsSource = products;
				}

				if (firebaseAPI.getUID() == "")
				{
					buyButton.Text = "Du måste logga in!";
					buyButton.IsEnabled = false;
				}

			}
			else {
				buyButton.IsVisible = false;
				loadData();
			}
		}

		void clickedRow(object sender, SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem != null)
			{
				Navigation.PushAsync(new ProductDetailPage(e.SelectedItem as product));
				productsList.SelectedItem = null;
			}
		}



		async void loadData()
		{
			products = new List<product>();

			JObject productsData = await firebaseAPI.doGet("mh2mall/product", "category", categoryName);

			if (productsData == null)
			{
				System.Diagnostics.Debug.WriteLine("NO DATA");
				return;
			}

			foreach (var prod in productsData)
			{
				System.Diagnostics.Debug.WriteLine("KEY: " + prod.Key);
				System.Diagnostics.Debug.WriteLine("VALUE: " + prod.Value);

				var prodInfo = prod.Value as JObject;
				System.Diagnostics.Debug.WriteLine((string)prodInfo["name"]);


				products.Add(new product() { category = categoryName, productId = prod.Key, name = (string)prodInfo["name"], price = (string)prodInfo["price"], image = (string)prodInfo["image"], description = (string)prodInfo["description"], favCounter = (int)prodInfo["favCounter"], major = (string)prodInfo["major"] });
			}

			productsList.ItemsSource = products;
		}


		async void buyFavs(object sender, EventArgs e)
		{ 
			/*
			var postData = new Dictionary<string, string>();
			postData.Add("userID", firebaseAPI.getUID());

			for (int i = 0; i < products.Count;i++)
			{
				postData.Add("product"+i.ToString(), products[i].name);
			}

			firebaseAPI.doPost(postData, "mh2mall/order");
			*/

			JObject userData = await firebaseAPI.doGet("mh2mall/users/" + firebaseAPI.getUID(), "", "");

			if (userData != null)
			{
				if (userData["userCard"] == null)
				{
					// INGET KORT. MATA IN KORTUPPGIFTER
					Navigation.PushAsync(new PayPage(totalAmount));
				}
				else {
					// KORT FINNS. TA BETALT.
					var answer = await DisplayAlert("Köpa", "Är du säker på att du vill köpa?", "Ja", "Avbryt");

					if (answer == true)
					{
						var customerID = (string)userData["userCard"];

						int stripeAmount = totalAmount * 100;

						var payResult = await stripeAPI.makeCustomerPayment(customerID, stripeAmount.ToString(), "sek", "");

						if (payResult == true)
						{
							// BETALNING OK
							products = new List<product>();
							productsList.ItemsSource = null;
							App.Current.Properties.Remove("fav");
							App.Current.SavePropertiesAsync();

							GoogleAnalytics.Current.Tracker.SendEvent("buy", "ok");

							foreach (var prod in products)
							{
								long prodPrice = Convert.ToInt64(prod.price)*1000;
								var trans = new Plugin.GoogleAnalytics.Abstractions.Model.TransactionItem(prod.productId, prod.name, prodPrice, 1);
								GoogleAnalytics.Current.Tracker.SendTransactionItem(trans);
							}


							DisplayAlert("Betalning", "Betalning genomförd!", "OK");
						}
						else {
							// BETALNING EJ OK
							GoogleAnalytics.Current.Tracker.SendEvent("buy", "fail");
							DisplayAlert("Betalning", "Betalning misslyckades!", "OK");
						}
					}
					else {
						GoogleAnalytics.Current.Tracker.SendEvent("buy", "cancel");
					}

				}
			}
		}


	}
}
