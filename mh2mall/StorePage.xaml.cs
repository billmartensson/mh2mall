﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using Plugin.GoogleAnalytics;

namespace mh2mall
{
	public partial class StorePage : ContentPage
	{
		public StorePage(store storeInfo)
		{
			InitializeComponent();

			GoogleAnalytics.Current.Tracker.SendView("InfoPage/"+storeInfo.name);

			storeName.Text = storeInfo.name;
			storeAddress.Text = storeInfo.address;
			storeImage.Source = storeInfo.image;
			storeOpen.Text = storeInfo.open;
		}
	}
}
