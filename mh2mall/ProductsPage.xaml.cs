﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using Newtonsoft.Json.Linq;

using Plugin.GoogleAnalytics;

namespace mh2mall
{
	public partial class ProductsPage : ContentPage
	{
		public ProductsPage()
		{
			InitializeComponent();

			GoogleAnalytics.Current.Tracker.SendView("Products");

			loadData2();
		}

		async void loadData2()
		{
			System.Diagnostics.Debug.WriteLine("LOAD DATA 2: ");
			JObject productsData = await firebaseAPI.doGet("mh2mall/product", "category", "Frukt");

			foreach (var productData in productsData)
			{
				System.Diagnostics.Debug.WriteLine("KEY: " + productData.Key);
				System.Diagnostics.Debug.WriteLine("VALUE: " + productData.Value);

				var productInfo = productData.Value as JObject;

				System.Diagnostics.Debug.WriteLine((string)productInfo["name"]);

				
			}
		}

		void goFav(object sender, EventArgs e)
		{
			Navigation.PushAsync(new ListProductsPage(""));
		}
		void goKitchen(object sender, EventArgs e)
		{
			Navigation.PushAsync(new ListProductsPage("Kök"));
		}
		void goSound(object sender, EventArgs e)
		{
			Navigation.PushAsync(new ListProductsPage("Ljud"));
		}
		void goMobile(object sender, EventArgs e)
		{
			Navigation.PushAsync(new ListProductsPage("Mobil"));
		}

	}
}
