﻿using System;

namespace mh2mall
{
	public class iapProduct
	{
		public String productId;
		public String productTitle;
		public float productPrice;
		public String productPriceFormatted;
		public String productDescription;
		public bool productBought;

		public iapProduct ()
		{
		}

		public iapProduct (String inProductId, String inProductTitle, float inProductPrice, String inProductPriceFormatted, String inProductDescription, bool inProductBought)
		{
			productId = inProductId;
			productTitle = inProductTitle;
			productPrice = inProductPrice;
			productPriceFormatted = inProductPriceFormatted;
			productDescription = inProductDescription;
			productBought = inProductBought;
		}

	}
}

