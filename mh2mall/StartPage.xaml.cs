﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using Newtonsoft.Json.Linq;

using Plugin.GoogleAnalytics;

namespace mh2mall
{
	public partial class StartPage : ContentPage
	{
		BeaconItem closestBeacon = null;

		product currentProduct = null;

		public StartPage()
		{
			InitializeComponent();

			GoogleAnalytics.Current.Tracker.SendView("Start");

			MessagingCenter.Subscribe<BeaconItem>(this, "beaconGone", (args) =>
			{
				//Console.WriteLine("BEACON EVENT");

				BeaconItem beacon = args as BeaconItem;

				if (closestBeacon == null)
				{
					return;
				}

				if (beacon.Major == closestBeacon.Major)
				{
					//beaconLabel.Text = "";
					//beaconLabel.BackgroundColor = Color.White;

					ibeaconBanner.IsVisible = false;

					closestBeacon = null;
				}

			});

			MessagingCenter.Subscribe<BeaconItem>(this, "beacon", (args) =>
			{
				//Console.WriteLine("BEACON EVENT");

				BeaconItem beacon = args as BeaconItem;

				if (closestBeacon == null)
				{
					closestBeacon = beacon;
					loadProduct();
				}

				if (beacon.CurrentDistance < closestBeacon.CurrentDistance)
				{
					closestBeacon = beacon;



					loadProduct();
				}

				System.Diagnostics.Debug.WriteLine(beacon.CurrentDistance.ToString() + " Major: " + beacon.Major);

				if (closestBeacon.Proximity == Proximity.Near)
				{
					System.Diagnostics.Debug.WriteLine("BEACONS NEAR");
					ibeaconBanner.IsVisible = true;
				}
				if (closestBeacon.Proximity == Proximity.Immediate)
				{
					System.Diagnostics.Debug.WriteLine("BEACONS Immediate");

					ibeaconBanner.IsVisible = true;

					if (currentProduct != null)
					{ 
						List<product> favProducts = new List<product>();

						if (App.Current.Properties.ContainsKey("fav"))
						{
							favProducts = App.Current.Properties["fav"] as List<product>;
						}

						product foundProd = null;

						foreach (product loopProduct in favProducts)
						{
							if (loopProduct.name == currentProduct.name)
							{
								foundProd = loopProduct;
							}
						}

						if (foundProd == null)
						{
							favProducts.Add(currentProduct);
							App.Current.Properties["fav"] = favProducts;
							App.Current.SavePropertiesAsync();
						}

					}	

				}
				if (closestBeacon.Proximity == Proximity.Far)
				{
					System.Diagnostics.Debug.WriteLine("BEACONS Far");
					ibeaconBanner.IsVisible = false;
				}
			});
		}

		async void loadProduct()
		{
			System.Diagnostics.Debug.WriteLine("LOAD PRODUCT "+closestBeacon.Major);

			JObject productsData = await firebaseAPI.doGet("mh2mall/product", "major", closestBeacon.Major);

			foreach (var prod in productsData)
			{
				System.Diagnostics.Debug.WriteLine("KEY: " + prod.Key);
				System.Diagnostics.Debug.WriteLine("VALUE: " + prod.Value);

				var prodInfo = prod.Value as JObject;
				System.Diagnostics.Debug.WriteLine((string)prodInfo["name"]);

				currentProduct = new product() { category = (string)prodInfo["category"], productId = prod.Key, name = (string)prodInfo["name"], price = (string)prodInfo["price"], image = (string)prodInfo["image"], description = (string)prodInfo["description"], favCounter = (int)prodInfo["favCounter"], major = (string)prodInfo["major"] };

				bannerName.Text = currentProduct.name;
				bannerPrice.Text = currentProduct.price+"kr";

				GoogleAnalytics.Current.Tracker.SendEvent("iBeacon", currentProduct.name);
			}
		}

		void readMore(object sender, EventArgs e)
		{
			Navigation.PushAsync(new ProductDetailPage(currentProduct));
		}
	}
}
