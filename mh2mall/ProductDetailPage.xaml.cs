﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using Plugin.GoogleAnalytics;

namespace mh2mall
{
	public partial class ProductDetailPage : ContentPage
	{
		product thisProduct;

		public ProductDetailPage(product prod)
		{
			InitializeComponent();

			GoogleAnalytics.Current.Tracker.SendView("Products/" + prod.category+"/"+prod.name);

			thisProduct = prod;

			productImage.Source = prod.image;
			productName.Text = prod.name;
			productPrice.Text = "Pris: " + prod.price + "kr";
			productDescription.Text = prod.description;

			List<product> favProducts = new List<product>();

			if (App.Current.Properties.ContainsKey("fav"))
			{
				favProducts = App.Current.Properties["fav"] as List<product>;
			}

			foreach (product loopProduct in favProducts)
			{
				if (loopProduct.name == thisProduct.name)
				{
					productFavButton.Text = "-";
				}
			}

		}

		async void doFav(object sender, EventArgs e)
		{
			/*
			App.Current.Properties.Add("email", "apa@apa.apa");
			App.Current.SavePropertiesAsync();

			App.Current.Properties["email"] as string;
			*/

			List<product> favProducts = new List<product>();

			if (App.Current.Properties.ContainsKey("fav"))
			{
				favProducts = App.Current.Properties["fav"] as List<product>;

				await App.Current.SavePropertiesAsync();
			}

			if (productFavButton.Text == "+")
			{
				GoogleAnalytics.Current.Tracker.SendEvent("fav", "ON", thisProduct.category+"/"+thisProduct.name);

				favProducts.Add(thisProduct);

				thisProduct.favCounter++;

				productFavButton.Text = "-";
			}
			else {
				GoogleAnalytics.Current.Tracker.SendEvent("fav", "OFF", thisProduct.category + "/" + thisProduct.name);
				productFavButton.Text = "+";

				product prodToRemove = new product();

				thisProduct.favCounter--;

				foreach (product loopProduct in favProducts)
				{
					if (loopProduct.name == thisProduct.name)
					{
						prodToRemove = loopProduct;
					}
				}
				favProducts.Remove(prodToRemove);
			}

			var postData = new Dictionary<string, string>();
			postData.Add("name", thisProduct.name);
			postData.Add("category", thisProduct.category);
			postData.Add("description", thisProduct.description);
			postData.Add("favCounter", thisProduct.favCounter.ToString());
			postData.Add("image", thisProduct.image);
			postData.Add("price", thisProduct.price);
			postData.Add("major", thisProduct.major);

			firebaseAPI.doPut(postData, "mh2mall/product/" + thisProduct.productId);


			if (App.Current.Properties.ContainsKey("fav"))
			{
				App.Current.Properties.Remove("fav");
				App.Current.Properties.Add("fav", favProducts);
			}
			else {
				App.Current.Properties.Add("fav", favProducts);
			}

			await App.Current.SavePropertiesAsync();

		}

	}
}
