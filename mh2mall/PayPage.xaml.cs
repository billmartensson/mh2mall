﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;

using Xamarin.Forms;

using Plugin.GoogleAnalytics;

namespace mh2mall
{
	public partial class PayPage : ContentPage
	{
		int totalAmount = 0;

		public PayPage(int amount)
		{
			InitializeComponent();

			totalAmount = amount;
		}

		async void letsPay(object sender, EventArgs e)
		{
			var stripeToken = await stripeAPI.getToken(cardNumber.Text, cardMonth.Text, cardYear.Text, cardCVC.Text);

			if (stripeToken == null)
			{
				// FELAKTIGT KORT
				DisplayAlert("Betalning", "Felaktigt kort", "OK");
				return;
			}

			var customerId = await stripeAPI.createCustomer(stripeToken, "", "");

			if (customerId == null)
			{
				// KUNDE INTE SKAPA KUND
				DisplayAlert("Betalning", "Registering misslyckades!", "OK");
				return;
			}

			int stripeAmount = totalAmount * 100;

			// TA BETALT
			var payResult = await stripeAPI.makeCustomerPayment(customerId, stripeAmount.ToString(), "sek", "");

			if (payResult == false)
			{
				// BETALNING MISSLYCKADES
				GoogleAnalytics.Current.Tracker.SendEvent("buy", "fail");
				DisplayAlert("Betalning", "Betalning misslyckades!", "OK");
				return;
			}

			// SPARA KORT/KUND PÅ FIREBASE
			JObject userData = await firebaseAPI.doGet("mh2mall/users/" + firebaseAPI.getUID(), "", "");

			if (userData != null)
			{
				var postData = new Dictionary<string, string>();
				postData.Add("userName", (string)userData["userName"]);
				postData.Add("userAddress", (string)userData["userAddress"]);
				postData.Add("userPhone", (string)userData["userPhone"]);
				postData.Add("userCard", customerId);

				await firebaseAPI.doPut(postData, "mh2mall/users/" + firebaseAPI.getUID());

				App.Current.Properties.Remove("fav");
				await App.Current.SavePropertiesAsync();
				GoogleAnalytics.Current.Tracker.SendEvent("buy", "ok");
				Navigation.PopAsync();
			}


		}

	}
}
