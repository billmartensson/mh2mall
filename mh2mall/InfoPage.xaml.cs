﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using Newtonsoft.Json.Linq;

using Plugin.GoogleAnalytics;

namespace mh2mall
{
	public class store
	{
		public string name { get; set; }
		public string address { get; set; }
		public string image { get; set; }
		public string open { get; set; }
	}


	public partial class InfoPage : ContentPage
	{
		List<store> stores = new List<store>();

		public InfoPage()
		{
			InitializeComponent();

			GoogleAnalytics.Current.Tracker.SendView("InfoPage");

			loadData();

			storesList.ItemSelected += clickedRow;
		}

		void clickedRow(object sender, SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem != null)
			{
				Navigation.PushAsync(new StorePage(e.SelectedItem as store));
				storesList.SelectedItem = null;
			}
		}

		async void loadData()
		{
			JObject storesData = await firebaseAPI.doGet("mh2mall/stores", "", "");

			foreach (var store in storesData)
			{ 
				System.Diagnostics.Debug.WriteLine("KEY: " + store.Key);
				System.Diagnostics.Debug.WriteLine("VALUE: " + store.Value);

				var storeInfo = store.Value as JObject;
				System.Diagnostics.Debug.WriteLine((string)storeInfo["name"]);

				var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

				string StoreOpen = "open";
				if (ci.TwoLetterISOLanguageName == "sv")
				{
					StoreOpen = "open_sv";
				}


				stores.Add(new store() { name = (string)storeInfo["name"], address = (string)storeInfo["address"], image = (string)storeInfo["image"], open = (string)storeInfo[StoreOpen] });
			}

			storesList.ItemsSource = stores;
		}
	}
}
