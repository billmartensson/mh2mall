﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace mh2mall
{
	static public class stripeAPI
	{
		static string stripeKey = "pk_test_l3GRbZMScdRT3OfNDZDhEanM";
		static string stripeSecret = "sk_test_rHHuUDlCGpCThZmonlQczTm0";

		static public async Task<string> getToken(string cardNumber, string expMonth, string expYear, string cvc)
		{ 
			var httpClientRequest = new HttpClient();

			var authData = string.Format("{0}:{1}", stripeKey, "x");
			var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

			httpClientRequest.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

			var values = new Dictionary<string, string>();
			values.Add("card[number]", cardNumber);
			values.Add("card[exp_month]", expMonth);
			values.Add("card[exp_year]", expYear);
			values.Add("card[cvc]", cvc);

			var content = new FormUrlEncodedContent(values);

			try
			{
				var result = await httpClientRequest.PostAsync("https://api.stripe.com/v1/tokens", content);
				var resultString = await result.Content.ReadAsStringAsync();

				//System.Diagnostics.Debug.WriteLine(resultString);

				var jsonResult = JObject.Parse(resultString);

				var token = (string)jsonResult["id"];

				//var cardData = jsonResult["card"];
				//var cardId = (string)cardData["id"];

				return token;
			}
			catch (Exception e) {
				System.Diagnostics.Debug.WriteLine(e.Message);
			}



			return null;
		}

		static public async Task<bool> makeTokenPayment(string token, string amount, string currency, string description)
		{
			var httpClientRequest = new HttpClient();

			var authData = string.Format("{0}:{1}", stripeSecret, "x");
			var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

			httpClientRequest.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

			var values = new Dictionary<string, string>();
			values.Add("amount", amount);
			values.Add("currency", currency);
			values.Add("description", description);
			values.Add("source", token);

			var content = new FormUrlEncodedContent(values);

			try
			{
				var result = await httpClientRequest.PostAsync("https://api.stripe.com/v1/charges", content);
				var resultString = await result.Content.ReadAsStringAsync();

				//System.Diagnostics.Debug.WriteLine(resultString);

				var jsonResult = JObject.Parse(resultString);

				var statusText = (string)jsonResult["status"];

				if (statusText == "succeeded")
				{
					return true;
				}
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e.Message);
			}



			return false;
		}

		static public async Task<bool> makeCustomerPayment(string customer, string amount, string currency, string description)
		{
			var httpClientRequest = new HttpClient();

			var authData = string.Format("{0}:{1}", stripeSecret, "x");
			var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

			httpClientRequest.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

			var values = new Dictionary<string, string>();
			values.Add("amount", amount);
			values.Add("currency", currency);
			values.Add("description", description);
			values.Add("customer", customer);

			var content = new FormUrlEncodedContent(values);

			try
			{
				var result = await httpClientRequest.PostAsync("https://api.stripe.com/v1/charges", content);
				var resultString = await result.Content.ReadAsStringAsync();

				//System.Diagnostics.Debug.WriteLine(resultString);

				var jsonResult = JObject.Parse(resultString);

				var statusText = (string)jsonResult["status"];

				if (statusText == "succeeded")
				{
					return true;
				}
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e.Message);
			}



			return false;
		}

		static public async Task<string> createCustomer(string token, string email, string description)
		{
			var httpClientRequest = new HttpClient();

			var authData = string.Format("{0}:{1}", stripeSecret, "x");
			var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

			httpClientRequest.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

			var values = new Dictionary<string, string>();
			values.Add("source", token);
			values.Add("email", email);
			values.Add("description", description);

			var content = new FormUrlEncodedContent(values);

			try
			{
				var result = await httpClientRequest.PostAsync("https://api.stripe.com/v1/customers", content);
				var resultString = await result.Content.ReadAsStringAsync();

				//System.Diagnostics.Debug.WriteLine(resultString);

				var jsonResult = JObject.Parse(resultString);

				var customerId = (string)jsonResult["id"];

				System.Diagnostics.Debug.WriteLine("MADE CUSTOMER "+customerId);

				return customerId;
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e.Message);
			}



			return null;
		}
	}
}
