﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace mh2mall
{
	public interface IIAP
	{
		Task<List<iapProduct>> loadProducts ();
		Task<bool> restoreProducts ();
		Task<bool> buyProduct (String productId);

	}
}

