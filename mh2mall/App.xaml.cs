﻿using Xamarin.Forms;
using System.Threading.Tasks;

using Plugin.GoogleAnalytics;

namespace mh2mall
{
	public partial class App : Application
	{
		static IBeaconLocater beaconLocater;

		public App()
		{
			InitializeComponent();

			GoogleAnalytics.Current.Config.TrackingId = "UA-89203420-1";
			GoogleAnalytics.Current.Config.AppId = "MH2";
			GoogleAnalytics.Current.Config.AppName = "MH2";
			GoogleAnalytics.Current.Config.AppVersion = "1.0.0.0";
			//GoogleAnalytics.Current.Config.Debug = true;
			//For tracking install and starts app, you can change default event properties:
			//GoogleAnalytics.Current.Config.ServiceCategoryName = "App";
			//GoogleAnalytics.Current.Config.InstallMessage = "Install";
			//GoogleAnalytics.Current.Config.StartMessage = "Start";
			//GoogleAnalytics.Current.Config.AppInstallerId = "someID"; // for custom installer id
			GoogleAnalytics.Current.InitTracker();

			//Track view
			GoogleAnalytics.Current.Tracker.SendView("AppStart");

			/*
			var trans = new Plugin.GoogleAnalytics.Abstractions.Model.TransactionItem("sku1", "Prod1", 1000, 2);
			GoogleAnalytics.Current.Tracker.SendTransactionItem(trans);
			*/

			beaconLocater = DependencyService.Get<IBeaconLocater>();
			beaconLocater.startBeacon("e2c56db5-dffb-48d2-b060-d0f5a71096e0", "mhbeacon", "MH iBeacon App", "Välkommen", "Hejdå");
			//beaconLocater.startBeacon("2a83690e-fd67-44de-a143-3e0aedb77689", "mhbeacon", "MH iBeacon App", "Välkommen", "Hejdå");
			checkBeacon();

			MainPage = new RootPage();
		}

		async Task<bool> checkBeacon()
		{
			while (true)
			{
				Device.BeginInvokeOnMainThread(() =>
				{
					var list = beaconLocater.GetAvailableBeacons();

					System.Diagnostics.Debug.WriteLine(list.Count);

					for (int i = 0; i < list.Count; i++)
					{
						if (list[i].gone)
						{
							MessagingCenter.Send(list[i], "beaconGone");
						}
						else {
							MessagingCenter.Send(list[i], "beacon");
						}
					}
				});

				await Task.Delay(1000);
			}
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
