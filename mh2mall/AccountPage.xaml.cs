﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using Newtonsoft.Json.Linq;

using Plugin.GoogleAnalytics;

namespace mh2mall
{
	public partial class AccountPage : ContentPage
	{
		public AccountPage()
		{
			InitializeComponent();

			GoogleAnalytics.Current.Tracker.SendView("Account");

			if (firebaseAPI.getUID() == "")
			{
				// Inte inloggad
				loginLayout.IsVisible = true;
			}
			else {
				loginLayout.IsVisible = false;

				getUserInfo();
			}
		}

		async void getUserInfo()
		{ 
			JObject userData = await firebaseAPI.doGet("mh2mall/users/"+firebaseAPI.getUID(), "", "");

			if (userData != null)
			{
				userName.Text = (string)userData["userName"];
				userAddress.Text = (string)userData["userAddress"];
				userPhone.Text = (string)userData["userPhone"];
			}
		}


		async void userLogin(object sender, EventArgs e)
		{
			var loginResult = await firebaseAPI.doLogin(emailEntry.Text, passwordEntry.Text);

			if (loginResult == false)
			{
				// KUNDE INTE LOGGA IN
				GoogleAnalytics.Current.Tracker.SendEvent("Login", "Fail");

				DisplayAlert(AppResources.General_Error, AppResources.Account_ErrorLogin, "OK");
			}
			else {
				// RÄTT INLOGGNING
				GoogleAnalytics.Current.Tracker.SendEvent("Login", "Ok");
				loginLayout.IsVisible = false;
				getUserInfo();
			}
		}

		async void userRegister(object sender, EventArgs e)
		{
			var registerResult = await firebaseAPI.doRegister(emailEntry.Text, passwordEntry.Text);

			if (registerResult == false)
			{
				// KUNDE INTE REGISTRERA
				GoogleAnalytics.Current.Tracker.SendEvent("Register", "Fail");
			}
			else {
				// RÄTT REGISTERING
				GoogleAnalytics.Current.Tracker.SendEvent("Register", "Ok");

				loginLayout.IsVisible = false;
				getUserInfo();
			}
		}

		async void saveUser(object sender, EventArgs e)
		{
			var postData = new Dictionary<string, string>();
			postData.Add("userName", userName.Text);
			postData.Add("userAddress", userAddress.Text);
			postData.Add("userPhone", userPhone.Text);

			firebaseAPI.doPut(postData, "mh2mall/users/"+firebaseAPI.getUID());

		}

		async void logoutUser(object sender, EventArgs e)
		{
			firebaseAPI.doLogout();
			loginLayout.IsVisible = true;
		}

	}
}
