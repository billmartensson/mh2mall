﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace mh2mall
{
	public partial class MenuPage : ContentPage
	{
		RootPage mainRoot;

		public MenuPage(RootPage root)
		{
			InitializeComponent();

			mainRoot = root;
		}

		void goStart(object sender, EventArgs e)
		{
			mainRoot.Detail = new NavigationPage(new StartPage());
			mainRoot.IsPresented = false;
		}

		void goProducts(object sender, EventArgs e)
		{
			mainRoot.Detail = new NavigationPage(new ProductsPage());
			mainRoot.IsPresented = false;
		}

		void goDigitalProducts(object sender, EventArgs e)
		{
			mainRoot.Detail = new NavigationPage(new DigitalProductsPage());
			mainRoot.IsPresented = false;
		}

		void goInfo(object sender, EventArgs e)
		{
			mainRoot.Detail = new NavigationPage(new InfoPage());
			mainRoot.IsPresented = false;
		}

		void goAccount(object sender, EventArgs e)
		{
			mainRoot.Detail = new NavigationPage(new AccountPage());
			mainRoot.IsPresented = false;
		}

	}
}
