﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using Newtonsoft.Json.Linq;

using Plugin.GoogleAnalytics;

namespace mh2mall
{
	public partial class DigitalProductsPage : ContentPage
	{
		List<iapProduct> inapp_products = new List<iapProduct>();
		IIAP inapp = DependencyService.Get<IIAP>();

		string premiumProductId = "mh2premium";
/*
		#if __IOS__
		string goldcoinProductId = "mh2goldcoin";
		#else
		string goldcoinProductId = "android.test.purchased";
		#endif
*/
		string goldcoinProductId = "mh2goldcoin";


		public DigitalProductsPage()
		{
			InitializeComponent();

			GoogleAnalytics.Current.Tracker.SendView("DigitalProduct");

			getIAP();
		}

		async void getIAP()
		{
			System.Diagnostics.Debug.WriteLine("GO PROD");
			inapp_products = await inapp.loadProducts();
			System.Diagnostics.Debug.WriteLine("DONE PROD");

			foreach (iapProduct product in inapp_products)
			{
				System.Diagnostics.Debug.WriteLine("PRODUCT: " + product.productId + " " + product.productTitle + " - " + product.productPriceFormatted + " " + product.productBought);

				if (product.productId == premiumProductId)
				{
					premiumTitleLabel.Text = product.productTitle;
					premiumDescriptionLabel.Text = product.productDescription;
					premiumPriceLabel.Text = product.productPriceFormatted;

					if (product.productBought)
					{
						buyButton.Text = "Redan köpt!";
						buyButton.IsEnabled = false;
					}
					else {
						buyButton.IsEnabled = true;
					}
				}
				// android.test.purchased
				if (product.productId == goldcoinProductId)
				{
					coinsTitleLabel.Text = product.productTitle;
					coinsDescriptionLabel.Text = product.productDescription;
					coinsPriceLabel.Text = product.productPriceFormatted;

					buyCoinsButton.IsEnabled = true;
				}


			}
		}

		async void buyNow(Object sender, EventArgs args)
		{
			bool buyOK = await inapp.buyProduct(premiumProductId);

			if (buyOK)
			{
				System.Diagnostics.Debug.WriteLine("KÖP OK");

				Application.Current.Properties["premium"] = true;
				Application.Current.SavePropertiesAsync();
				bonusButton.IsEnabled = true;

				premiumOK();
			}
			else {
				System.Diagnostics.Debug.WriteLine("KÖP EJ OK");
			}
		}

		async void buyCoinsNow(Object sender, EventArgs args)
		{
			if (firebaseAPI.getUID() == "")
			{

				return;
			}

			bool buyOK = await inapp.buyProduct(goldcoinProductId);

			if (buyOK)
			{
				System.Diagnostics.Debug.WriteLine("KÖP OK");


				JObject userData = await firebaseAPI.doGet("mh2mall/users/" + firebaseAPI.getUID(), "", "");

				if (userData != null)
				{
					var postData = new Dictionary<string, string>();
					postData.Add("userName", (string)userData["userName"]);
					postData.Add("userAddress", (string)userData["userAddress"]);
					postData.Add("userPhone", (string)userData["userPhone"]);
					postData.Add("userCard", (string)userData["userCard"]);

					int userCoins = 0;
					if (userData["coins"] != null)
					{
						userCoins = (int)userData["coins"];
					}
					userCoins = userCoins + 1;

					postData.Add("coins", userCoins.ToString());

					if (userData["premium"] != null)
					{
						postData.Add("premium", (string)userData["premium"]);
					}


					await firebaseAPI.doPut(postData, "mh2mall/users/" + firebaseAPI.getUID());
				}

			}
			else {
				System.Diagnostics.Debug.WriteLine("KÖP EJ OK");
			}
		}

		async void restoreNow(Object sender, EventArgs args)
		{
			await inapp.restoreProducts();

			inapp_products = await inapp.loadProducts();
			System.Diagnostics.Debug.WriteLine("DONE RESTORE");

			foreach (iapProduct product in inapp_products)
			{
				System.Diagnostics.Debug.WriteLine("PRODUCT: " + product.productTitle + " - " + product.productPriceFormatted + " " + product.productBought);

				if (product.productId == premiumProductId)
				{
					if (product.productBought)
					{
						buyButton.Text = "Redan köpt!";

						premiumOK();

						Application.Current.Properties["premium"] = true;
						Application.Current.SavePropertiesAsync();
						bonusButton.IsEnabled = true;

						buyButton.IsEnabled = false;
					}
					else {
						buyButton.IsEnabled = true;
					}
				}

			}

		}

		async void premiumOK()
		{ 
			JObject userData = await firebaseAPI.doGet("mh2mall/users/" + firebaseAPI.getUID(), "", "");

			if (userData != null)
			{
				var postData = new Dictionary<string, string>();
				postData.Add("userName", (string)userData["userName"]);
				postData.Add("userAddress", (string)userData["userAddress"]);
				postData.Add("userPhone", (string)userData["userPhone"]);
				postData.Add("userCard", (string)userData["userCard"]);

				int userCoins = 0;
				if (userData["coins"] != null)
				{
					userCoins = (int)userData["coins"];
				}

				postData.Add("coins", userCoins.ToString());

				postData.Add("premium", "true");

				await firebaseAPI.doPut(postData, "mh2mall/users/" + firebaseAPI.getUID());
			}
		}

	}
}
