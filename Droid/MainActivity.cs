﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

// IN APP BILLING
using Xamarin.InAppBilling;
using Xamarin.InAppBilling.Utilities;
// IN APP BILLING END

namespace mh2mall.Droid
{
	[Activity(Label = "mh2mall.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		// IN APP BILLING
		public static MainActivity instance = null;
		public static InAppBillingServiceConnection _serviceConnection;
		// IN APP BILLING END

		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			string[] PermissionsLocation =
			{
						Android.Manifest.Permission.AccessCoarseLocation,
			  Android.Manifest.Permission.AccessFineLocation
			};
			const int RequestLocationId = 0;

			if (Build.VERSION.SdkInt >= Build.VERSION_CODES.M)
			{

				if (ShouldShowRequestPermissionRationale(Android.Manifest.Permission.AccessFineLocation))
				{
					RequestPermissions(PermissionsLocation, RequestLocationId);
				}
			}


			// IN APP BILLING START
			string publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyy9rAYUeSCjDtg+1BDZFFlM9iQJ/lrSP4qs1sJsCCLwR8ecO4cE+vprV3ESUzyEMKsbLR0U0Fn7uA+rl+yrkbJbmU87IMLj2PE4+IWcBy+1sjvEfh9/JtMh4WiMrcRHRTRlOxtfNMQtBg6IDp0R93DklJYTUeQhnQp431teQ7ibu5ELnAwr67hhgz6pgtRUFi3Sz9f/M6pGF25Yg5ZwLJXM20a7MK+Z2XoenM5oDNUntbSkP0qqR0prLOuf61WY++bR12dgdv9Co1CzSYLnr0x18ribvNlS0B898izXcZlrAEQS1c0OZXrjxUdSfPCK5uU2NhrC5ZUO9LCy7cDoBDQIDAQAB";

			_serviceConnection = new InAppBillingServiceConnection(this, publicKey);
			_serviceConnection.OnConnected += () =>
			{
				Console.WriteLine("IN APP BILLING CONNECTED");
				_serviceConnection.BillingHandler.OnProductPurchased += (int response, Purchase purchase, string purchaseData, string purchaseSignature) =>
				{
					Console.WriteLine("OnProductPurchased");
					//buyResult = 1;
				};
				_serviceConnection.BillingHandler.OnProductPurchasedError += (int responseCode, string sku) =>
				{
					Console.WriteLine("OnProductPurchasedError");
					//buyResult = 2;
				};
				_serviceConnection.BillingHandler.OnUserCanceled += () =>
				{
					Console.WriteLine("OnUserCanceled");
					//buyResult = 2;
				};
				_serviceConnection.BillingHandler.OnPurchaseConsumed += (string token) =>
				{
					Console.WriteLine("OnPurchaseConsumed");

				};
				_serviceConnection.BillingHandler.OnPurchaseConsumedError += (int responseCode, string token) =>
				{
					Console.WriteLine("OnPurchaseConsumedError");

				};

				_serviceConnection.OnInAppBillingError += (error, message) =>
				{
					Console.WriteLine("OnInAppBillingError " + message);
				};
			};

			_serviceConnection.Connect();
			// IN APP BILLING END

			global::Xamarin.Forms.Forms.Init(this, bundle);

			LoadApplication(new App());
		}
	}
}
