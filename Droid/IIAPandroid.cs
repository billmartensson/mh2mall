﻿using System;
using System.Threading.Tasks;
using System.Threading.Tasks;
using System.Collections.Generic;

using mh2mall;
using mh2mall.Droid;

using Xamarin.Forms;
using Xamarin.InAppBilling;
using Xamarin.InAppBilling.Utilities;


[assembly: Dependency (typeof(IIAPandroid))]
namespace mh2mall.Droid
{
	public class IIAPandroid : IIAP
	{
		//public static InAppBillingServiceConnection _serviceConnection;

		private IList<Product> _products;

		List<iapProduct> inapp_products = new List<iapProduct> ();

		int buyResult = 0;

		public IIAPandroid ()
		{
			Console.WriteLine("IIAPandroid CREATED");

			string publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyy9rAYUeSCjDtg+1BDZFFlM9iQJ/lrSP4qs1sJsCCLwR8ecO4cE+vprV3ESUzyEMKsbLR0U0Fn7uA+rl+yrkbJbmU87IMLj2PE4+IWcBy+1sjvEfh9/JtMh4WiMrcRHRTRlOxtfNMQtBg6IDp0R93DklJYTUeQhnQp431teQ7ibu5ELnAwr67hhgz6pgtRUFi3Sz9f/M6pGF25Yg5ZwLJXM20a7MK+Z2XoenM5oDNUntbSkP0qqR0prLOuf61WY++bR12dgdv9Co1CzSYLnr0x18ribvNlS0B898izXcZlrAEQS1c0OZXrjxUdSfPCK5uU2NhrC5ZUO9LCy7cDoBDQIDAQAB";

			/*
			MainActivity._serviceConnection.OnConnected += () => {
				Console.WriteLine("IN APP BILLING CONNECTED");
				MainActivity._serviceConnection.BillingHandler.OnProductPurchased += (int response, Purchase purchase, string purchaseData, string purchaseSignature) => {
					Console.WriteLine("OnProductPurchased");
					buyResult = 1;
				};
				MainActivity._serviceConnection.BillingHandler.OnProductPurchasedError += (int responseCode, string sku) => {
					Console.WriteLine("OnProductPurchasedError");
					buyResult = 2;
				};
				MainActivity._serviceConnection.BillingHandler.OnUserCanceled += () => {
					Console.WriteLine("OnUserCanceled");
					buyResult = 2;
				};
				MainActivity._serviceConnection.BillingHandler.OnPurchaseConsumed += (string token) => {
					Console.WriteLine("OnPurchaseConsumed");

				};
				MainActivity._serviceConnection.BillingHandler.OnPurchaseConsumedError += (int responseCode, string token) => {
					Console.WriteLine("OnPurchaseConsumedError");

				};
			};
			*/
		}

		public async Task<List<iapProduct>> loadProducts ()
		{
			int loopCounter = 0;
			while(MainActivity._serviceConnection.Connected == false)
			{
				Console.WriteLine ("WAITING FOR CONNECT!!");
				await Task.Delay (500);

				loopCounter++;

				if(loopCounter == 50)
				{
					return null;
				}
			}

			_products = await MainActivity._serviceConnection.BillingHandler.QueryInventoryAsync (new List<string> {
				ReservedTestProductIDs.Purchased,
				ReservedTestProductIDs.Canceled,
				ReservedTestProductIDs.Refunded,
				ReservedTestProductIDs.Unavailable,
				"mh2premium"
			}, ItemType.Product);

			inapp_products.Clear ();

			foreach(Product thisProd in _products)
			{
				Console.WriteLine (thisProd);

				inapp_products.Add(new iapProduct(thisProd.ProductId, thisProd.Title, 0, thisProd.Price, thisProd.Description, false));
			}


			return inapp_products;
		}

		public async Task<bool> restoreProducts ()
		{
			Console.WriteLine ("RESTORE");
			var purchases = MainActivity._serviceConnection.BillingHandler.GetPurchases (ItemType.Product);

			Console.WriteLine (purchases.Count);

			foreach (Purchase thisPur in purchases) {
				Console.WriteLine (thisPur);

				for(int i = 0;i < inapp_products.Count;i++)
				{
					if(inapp_products[i].productId == thisPur.ProductId)
					{
						inapp_products [i].productBought = true;
					}
				}
			}

			return true;
		}

		public async Task<bool> buyProduct (String productId)
		{
			buyResult = -1;
			foreach(Product thisProd in _products)
			{
				if(thisProd.ProductId == productId)
				{
					buyResult = 0;

					MainActivity.instance.RunOnUiThread(()=>{
						MainActivity._serviceConnection.BillingHandler.BuyProduct (thisProd);
					});

					/*
					MainActivity.instance.RunOnUiThread(()=>{
						MainActivity._serviceConnection.BillingHandler.BuyProduct (thisProd);
					});
					*/



					//MainActivity.buyProduct (thisProd);

				}
			}

			if(buyResult == -1)
			{
				return false;
			}

			int loopCounter = 0;
			while(buyResult == 0)
			{
				await Task.Delay (500);
				loopCounter++;
				if(loopCounter > 10000)
				{
					return false;
				}
			}

			if(buyResult == 1)
			{
				return true;
			}

			return false;
		}

	}
}

