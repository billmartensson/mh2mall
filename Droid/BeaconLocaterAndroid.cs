﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using mh2mall.Droid;
using Android.App;
using RadiusNetworks.IBeaconAndroid;
using mh2mall;
using Android.Content;
using Android.Support.V4.App;

using Android.OS;
using Xamarin.Forms.Platform.Android;
using Android.Runtime;


[assembly: Dependency (typeof (BeaconLocaterAndroid))]

namespace mh2mall.Droid
{
	public class BeaconLocaterAndroid : Java.Lang.Object, IBeaconLocater, IBeaconConsumer
	{
		string uuid = "8deefbb9-f738-4297-8040-96668bb44281";
		string beaconId = "OfficeBeacon";

		IBeaconManager _iBeaconManager;
		MonitorNotifier _monitorNotifier;
		RangeNotifier _rangeNotifier;
		Region _monitoringRegion;
		Region _rangingRegion;
		Context context;
		bool paused;
		List<BeaconItem> beacons;

		String beaconNotificationTitle;
		String beaconEnterNotificationText;
		String beaconExitNotificationText;
		DateTime lastEnterNotification;
		DateTime lastExitNotification;

		public BeaconLocaterAndroid ()
		{
			beacons = new List<BeaconItem> ();



			//for testing
//			beacons = new List<BeaconItem> {
//				new BeaconItem { Name = "Android1", Minor = "1233", CurrentDistance = 0.5 },
//				new BeaconItem { Name = "Android2", Minor = "1234", CurrentDistance = 0.2 },
//				new BeaconItem { Name = "Android3", Minor = "1235", CurrentDistance = 12.5 },
//			};



		}

		public List<BeaconItem> GetAvailableBeacons() {
			for(int i = 0;i < beacons.Count;i++)
			{
				if((DateTime.Now - beacons[i].lastSeen).TotalSeconds > 10)
				{
					if(beacons [i].gone == false)
					{
						beacons [i].gone = true;
						beacons [i].goneTime = DateTime.Now;
					}
				} 
				if((DateTime.Now - beacons[i].goneTime).TotalSeconds > 10 && beacons[i].gone)
				{
					Console.WriteLine ("REMOVE BEACONS GONE");
					beacons.Remove(beacons[i]);
				} 
			}
			return beacons;
		}

		public void PauseTracking() {
			paused = true;
		}

		public void ResumeTracking() {
			paused = false;
		}

		public void setTracking (bool track)
		{
		
		}

		public void startBeacon (String buuid, String bid, String notifTitle, String enterNotifText, String exitNotifText)
		{
			uuid = buuid;
			beaconId = bid;
			beaconNotificationTitle = notifTitle;
			beaconEnterNotificationText = enterNotifText;
			beaconExitNotificationText = exitNotifText;

			context = Xamarin.Forms.Forms.Context;

			_iBeaconManager = IBeaconManager.GetInstanceForApplication (context);
			_monitorNotifier = new MonitorNotifier ();
			_rangeNotifier = new RangeNotifier ();

			_monitoringRegion = new Region (beaconId, uuid, null, null);
			_rangingRegion = new Region(beaconId, uuid, null, null);

			_iBeaconManager.Bind (this);

			_monitorNotifier.EnterRegionComplete += enterRegionEvent;
			_monitorNotifier.ExitRegionComplete += exitRegionEvent;

			_rangeNotifier.DidRangeBeaconsInRegionComplete += RangingBeaconsInRegion;
		}

		void enterRegionEvent(Object sender, MonitorEventArgs arg)
		{
			Console.WriteLine ("REGION ENTER");

			if(beaconEnterNotificationText != "")
			{
				Intent intent = new Intent (context, typeof(MainActivity));
				const int pendingIntentId = 0;
				PendingIntent pendingIntent = 
					PendingIntent.GetActivity (context, pendingIntentId, intent, PendingIntentFlags.OneShot);

				Notification.Builder builder = new Notification.Builder (context)
					.SetContentIntent (pendingIntent)
					.SetContentTitle (beaconNotificationTitle)
					.SetContentText (beaconEnterNotificationText)
					.SetSmallIcon (Resource.Drawable.icon);

				// Build the notification:
				Notification notification = builder.Build();

				// Get the notification manager:
				NotificationManager notificationManager = context.GetSystemService (Context.NotificationService) as NotificationManager;

				// Publish the notification:
				const int notificationId = 0;
				notificationManager.Notify (notificationId, notification);
			}
		}
		void exitRegionEvent(Object sender, MonitorEventArgs arg)
		{
			Console.WriteLine ("REGION EXIT");

			if(beaconExitNotificationText != "")
			{
				Intent intent = new Intent (context, typeof(MainActivity));
				const int pendingIntentId = 0;
				PendingIntent pendingIntent = 
					PendingIntent.GetActivity (context, pendingIntentId, intent, PendingIntentFlags.OneShot);

				Notification.Builder builder = new Notification.Builder (context)
					.SetContentIntent (pendingIntent)
					.SetContentTitle (beaconNotificationTitle)
					.SetContentText (beaconExitNotificationText)
					.SetSmallIcon (Resource.Drawable.icon);

				// Build the notification:
				Notification notification = builder.Build();

				// Get the notification manager:
				NotificationManager notificationManager = context.GetSystemService (Context.NotificationService) as NotificationManager;

				// Publish the notification:
				const int notificationId = 0;
				notificationManager.Notify (notificationId, notification);
			}

		}

		void RangingBeaconsInRegion(object sender, RangeEventArgs e)
		{
			if (e.Beacons.Count > 0)
			{
				foreach (var b in e.Beacons) {
					if ((ProximityType)b.Proximity != ProximityType.Unknown) {

						var exists = false;
						for (int i=0; i<beacons.Count; i++) {
							if (beacons[i].Minor.Equals(b.Minor.ToString())) {
								beacons[i].CurrentDistance = Math.Round (b.Accuracy, 2);
								SetProximity (b, beacons [i]);
								beacons [i].lastSeen = DateTime.Now;
								beacons [i].gone = false;
								exists = true;
							}
						}

						if (!exists) {
							var newBeacon = new BeaconItem {
								Minor = b.Minor.ToString (),
								Major = b.Major.ToString(), 
								Name = "",
								CurrentDistance = Math.Round (b.Accuracy, 2),
								gone = false,
								lastSeen = DateTime.Now
							};
							SetProximity (b, newBeacon);
							beacons.Add (newBeacon);
						}
					}
				}
			}
		}

		void SetProximity(IBeacon source, BeaconItem dest) {

			Proximity p = Proximity.Unknown;

			switch((ProximityType)source.Proximity) {
			case ProximityType.Immediate:
				p = Proximity.Immediate;
				break;
			case ProximityType.Near:
				p = Proximity.Near;
				break;
			case ProximityType.Far:
				p = Proximity.Far;
				break;
			}

			if (p > dest.Proximity || p < dest.Proximity) {
				dest.ProximityChangeTimestamp = DateTime.Now;
			} 

			dest.Proximity = p;
		}

		public void OnIBeaconServiceConnect()
		{
			_iBeaconManager.SetMonitorNotifier(_monitorNotifier);
			_iBeaconManager.SetRangeNotifier(_rangeNotifier);

			_iBeaconManager.StartMonitoringBeaconsInRegion(_monitoringRegion);
			_iBeaconManager.StartRangingBeaconsInRegion(_rangingRegion);


		}

		public Context ApplicationContext {
			get {return this.context;}
		}

		public bool BindService(Intent intent, IServiceConnection connection, Bind bind) {
			return context.BindService (intent, connection, bind);
		}

		public void UnbindService(IServiceConnection connection) {
			context.UnbindService (connection);
		}
	}
}

