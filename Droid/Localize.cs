﻿using System;
using Xamarin.Forms;

[assembly:Dependency(typeof(mh2mall.Droid.Localize))]

namespace mh2mall.Droid
{
	public class Localize : mh2mall.ILocalize
	{
		public System.Globalization.CultureInfo GetCurrentCultureInfo ()
		{
			var androidLocale = Java.Util.Locale.Default;
			var netLanguage = androidLocale.ToString().Replace ("_", "-"); // turns pt_BR into pt-BR
			return new System.Globalization.CultureInfo(netLanguage);
		}
	}
}